# Wallet
This is the demo application.


## Getting started
Install the docker service on your machine.

From the command prompt/console go to the directory devops in project root and then run the command:
docker-compose up -d

Two containers for postgres and kafka will be up and running as a result.
Postgres DB username is postgres.
Postgres DB password is postgres.
Kafka UI can be launched from http://localhost:3040

The file docs/wallet.drawio contains the DB structure. Please use the website https://draw.io for edit

