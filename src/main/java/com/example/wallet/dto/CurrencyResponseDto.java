package com.example.wallet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Project: wallet
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 23.02.2024 23:18 |
 * Created with IntelliJ IDEA
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyResponseDto {

    private String id;

    private String name;

    private Boolean isActive;
}
