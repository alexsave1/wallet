package com.example.wallet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransferResponseDto {
    private Long id;
    private Long debitAccount;
    private String debitCurrency;
    private BigDecimal debitAmount;
    private BigDecimal debitExchangeRate;
    private Long creditAccount;
    private String creditCurrency;
    private BigDecimal creditAmount;
    private BigDecimal creditExchangeRate;
    private String currency;
    private BigDecimal amount;
    private String status;
    private String message;
}
