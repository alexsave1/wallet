package com.example.wallet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransferRequestDto {
    private Long transferId;
    private Long profileId;
    private Long debitAccountId;
    private Long creditAccountId;
    private String currency;
    private BigDecimal amount;
}
