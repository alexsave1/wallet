package com.example.wallet.repository;

import com.example.wallet.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

    List<Account> getAccountsByProfileId(Long profileId);

    void deleteAccountsByProfileId(Long profileId);

    void deleteAccountsByCurrencyId(String currencyId);
}
