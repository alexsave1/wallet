package com.example.wallet.repository;

import com.example.wallet.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    Optional<Currency> findById(String id);

    void deleteById(String id);
}
