package com.example.wallet.controller;

import com.example.wallet.dto.ProfileRequestDto;
import com.example.wallet.dto.ProfileResponseDto;
import com.example.wallet.service.ProfileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ProfileController {

    private final ProfileService profileService;

    @PostMapping("/profile")
    public ResponseEntity<ProfileResponseDto> create(@RequestBody ProfileRequestDto requestDto) {
        log.info("Creating a new profile: {}", requestDto);
        ProfileResponseDto profileResponseDto = profileService.save(requestDto);
        log.info("Profile created successfully.");
        return new ResponseEntity<>(profileResponseDto, HttpStatus.CREATED);
    }

    @PutMapping("/profile/{id}")
    public ResponseEntity<ProfileResponseDto> update(@PathVariable(name = "id") Long id,
                                                      @RequestBody ProfileRequestDto requestDto) {
        log.info("Updating profile with ID {}: {}", id, requestDto);
        if (profileService.findById(id) == null) {
            log.warn("Profile with ID {} not found. Update failed", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        ProfileResponseDto updatedResponseDto = profileService.update(id, requestDto);
        if (updatedResponseDto == null) {
            log.warn("Invalid request for profile with ID {}. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info("Profile updated successfully.");
        return new ResponseEntity<>(updatedResponseDto, HttpStatus.OK);
    }

    @GetMapping("/profile/{id}")
    public ResponseEntity<ProfileResponseDto> get(@PathVariable(name = "id") Long id) {
        log.info("Getting profile with ID {}.", id);
        ProfileResponseDto profileResponseDto = profileService.findById(id);
        if (profileResponseDto == null) {
            log.warn("Profile with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Profile with ID {} getting successfully.", id);
        return new ResponseEntity<>(profileResponseDto, HttpStatus.OK);
    }

    @GetMapping("/profiles")
    public ResponseEntity<List<ProfileResponseDto>> getAll() {
        log.info("Get all profiles");
        List<ProfileResponseDto> profileResponseDtos = profileService.findAll();
        if (!profileResponseDtos.isEmpty()) {
            log.info("Get {} profiles successfully.", profileResponseDtos.size());
            return new ResponseEntity<>(profileResponseDtos, HttpStatus.OK);
        } else {
            log.warn("Get profiles failed.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Удаляются также счета, привязанные к профилю
     * @param id - id of the profile
     * @return profile response dto
     */
    @DeleteMapping("/profile/{id}")
    public ResponseEntity<ProfileResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting profile with ID {}.", id);
        if (profileService.findById(id) == null) {
            log.warn("Profile with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        profileService.delete(id);
        log.info("Profile with ID {} deleted successfully.", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
