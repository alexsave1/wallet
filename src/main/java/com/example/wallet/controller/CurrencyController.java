package com.example.wallet.controller;

import com.example.wallet.dto.CurrencyRequestDto;
import com.example.wallet.dto.CurrencyResponseDto;
import com.example.wallet.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyService currencyService;

    @PostMapping("/currency")
    public ResponseEntity<CurrencyResponseDto> create(@RequestBody CurrencyRequestDto requestDto) {
        log.info("Creating a new currency: {}", requestDto);
        CurrencyResponseDto currencyResponseDto = currencyService.save(requestDto);
        log.info("Currency created successfully.");
        return new ResponseEntity<>(currencyResponseDto, HttpStatus.CREATED);
    }

    @PutMapping("/currency/{id}")
    public ResponseEntity<CurrencyResponseDto> update(@PathVariable(name = "id") String id,
                                                      @RequestBody CurrencyRequestDto requestDto) {
        log.info("Updating currency with ID {}: {}", id, requestDto);
        if (currencyService.findById(id) == null) {
            log.warn("Currency with ID {} not found. Update failed", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        CurrencyResponseDto updatedResponseDto = currencyService.update(id, requestDto);
        if (updatedResponseDto == null) {
            log.warn("Invalid request for currency with ID {}. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info("Currency updated successfully.");
        return new ResponseEntity<>(updatedResponseDto, HttpStatus.OK);
    }

    @GetMapping("/currency/{id}")
    public ResponseEntity<CurrencyResponseDto> get(@PathVariable(name = "id") String id) {
        log.info("Getting currency with ID {}.", id);
        CurrencyResponseDto currencyResponseDto = currencyService.findById(id);
        if (currencyResponseDto == null) {
            log.warn("Currency with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Currency with ID {} getting successfully.", id);
        return new ResponseEntity<>(currencyResponseDto, HttpStatus.OK);
    }

    @GetMapping("/currencies")
    public ResponseEntity<List<CurrencyResponseDto>> getAll() {
        log.info("Get all currencies");
        List<CurrencyResponseDto> currencyResponseDtos = currencyService.findAll();
        if (!currencyResponseDtos.isEmpty()) {
            log.info("Get {} currencies successfully.", currencyResponseDtos.size());
            return new ResponseEntity<>(currencyResponseDtos, HttpStatus.OK);
        } else {
            log.warn("Get currencies failed.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/currency/{id}")
    public ResponseEntity<CurrencyResponseDto> delete(@PathVariable(name = "id") String id) {
        log.info("Deleting currency with ID {}.", id);
        if (currencyService.findById(id) == null) {
            log.warn("Currency with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        currencyService.delete(id);
        log.info("Currency with ID {} deleted successfully.", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
