package com.example.wallet.controller;

import com.example.wallet.dto.TransferRequestDto;
import com.example.wallet.kafka.KafkaProducer;
import com.example.wallet.service.TransferService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransferController {


//    private final KafkaProducer kafkaProducer;
    private final TransferService transferService;

    public TransferController(KafkaProducer kafkaProducer, TransferService transferService) {
//        this.kafkaProducer = kafkaProducer;
        this.transferService = transferService;
    }

    @PostMapping("/transfer")
    public void sendMessageToKafka(@RequestBody TransferRequestDto transferRequestDto) throws JsonProcessingException {
        transferService.transfer(transferRequestDto);
    }
//    @PostMapping("/transfer")
//    public String sendMessageToKafka(@RequestBody String message) {
//        return "Message sent to Kafka! " + kafkaProducer.sendMessage(message);
//    }
}
