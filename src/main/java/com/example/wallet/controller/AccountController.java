package com.example.wallet.controller;

import com.example.wallet.dto.AccountRequestDto;
import com.example.wallet.dto.AccountResponseDto;
import com.example.wallet.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/account")
    public ResponseEntity<AccountResponseDto> create(@RequestBody AccountRequestDto requestDto) {
        log.info("Creating a new account: {}", requestDto);
        AccountResponseDto accountResponseDto = accountService.save(requestDto);
        log.info("Account created successfully.");
        return new ResponseEntity<>(accountResponseDto, HttpStatus.CREATED);
    }

    @PutMapping("/account/{id}")
    public ResponseEntity<AccountResponseDto> update(@PathVariable(name = "id") Long id,
                                                     @RequestBody AccountRequestDto requestDto) {
        log.info("Updating account with ID {}: {}", id, requestDto);
        if (accountService.findById(id) == null) {
            log.warn("Account with ID {} not found. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        AccountResponseDto updatedResponseDto = accountService.update(id, requestDto);
        if (updatedResponseDto == null) {
            log.warn("Invalid request for account with ID {}. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info("Account with ID {} updated successfully.", id);
        return new ResponseEntity<>(updatedResponseDto, HttpStatus.OK);
    }

    @GetMapping("/account/{id}")
    public ResponseEntity<AccountResponseDto> get(@PathVariable Long id) {
        log.info("Getting account with ID {}.", id);
        AccountResponseDto accountResponseDto = accountService.findById(id);
        if (accountResponseDto == null) {
            log.warn("Account with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Account with ID {} getting successfully.", id);
        return new ResponseEntity<>(accountResponseDto, HttpStatus.OK);
    }

    @GetMapping("/accounts")
    public ResponseEntity<List<AccountResponseDto>> getAll() {
        log.info("Get all accounts");
        List<AccountResponseDto> accountResponseDtos = accountService.findAll();
        if (!accountResponseDtos.isEmpty()) {
            log.info("Get {} users successfully.", accountResponseDtos.size());
            return new ResponseEntity<>(accountResponseDtos, HttpStatus.OK);
        } else {
            log.warn("Get accounts failed.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/account/{id}")
    public ResponseEntity<AccountResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting account with ID {}.", id);
        if (accountService.findById(id) == null) {
            log.warn("Account with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        accountService.delete(id);
        log.info("Account with ID {} deleted successfully.", id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
