package com.example.wallet.service;

import com.example.wallet.dto.ProfileRequestDto;
import com.example.wallet.dto.ProfileResponseDto;

import java.util.List;

public interface ProfileService {

    ProfileResponseDto save(ProfileRequestDto requestDto);

    ProfileResponseDto update(Long id, ProfileRequestDto requestDto);

    ProfileResponseDto findById(Long id);

    List<ProfileResponseDto> findAll();

    void delete(Long id);
}
