package com.example.wallet.service;

import com.example.wallet.dto.CurrencyRequestDto;
import com.example.wallet.dto.CurrencyResponseDto;

import java.util.List;

public interface CurrencyService {
    CurrencyResponseDto save(CurrencyRequestDto requestDto);

    CurrencyResponseDto update(String id, CurrencyRequestDto requestDto);

    CurrencyResponseDto findById(String id);

    List<CurrencyResponseDto> findAll();

    void delete(String id);
}
