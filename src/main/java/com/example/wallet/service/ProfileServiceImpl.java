package com.example.wallet.service;

import com.example.wallet.dto.ProfileRequestDto;
import com.example.wallet.dto.ProfileResponseDto;
import com.example.wallet.mapper.ProfileMapper;
import com.example.wallet.model.Profile;
import com.example.wallet.repository.AccountRepository;
import com.example.wallet.repository.ProfileRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository profileRepository;
    private final AccountRepository accountRepository;
    private final ProfileMapper mapper;

    @Override
    public ProfileResponseDto save(ProfileRequestDto requestDto) {
        Profile profile = mapper.requestDtoToEntity(requestDto);

        profileRepository.save(profile);
        return mapper.entityToResponseDto(profile);
    }

    @Override
    public ProfileResponseDto update(Long id, ProfileRequestDto requestDto) {
        Profile profile = mapper.requestDtoToEntity(requestDto);
        profile.setId(id);
        profileRepository.save(profile);
        return mapper.entityToResponseDto(profile);
    }

    @Override
    public ProfileResponseDto findById(Long id) {
        return mapper.entityToResponseDto(
                profileRepository.findById(id).orElse(null));
    }

    @Override
    public List<ProfileResponseDto> findAll() {
        return profileRepository.findAll()
                .stream()
                .map(mapper::entityToResponseDto)
                .toList();
    }

    @Override
    public void delete(Long id) {
        accountRepository.deleteAccountsByProfileId(id);

        profileRepository.deleteById(id);
    }
}
