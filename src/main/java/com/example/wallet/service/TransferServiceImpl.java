package com.example.wallet.service;

import com.example.wallet.dto.TransferRequestDto;
import com.example.wallet.dto.TransferResponseDto;
import com.example.wallet.kafka.KafkaProducer;
import com.example.wallet.model.Transfer;
import com.example.wallet.repository.TransferRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class TransferServiceImpl implements TransferService{
    private final TransferRepository transferRepository;
    private final KafkaProducer kafkaProducer;

    public TransferServiceImpl(TransferRepository transferRepository, KafkaProducer kafkaProducer) {
        this.transferRepository = transferRepository;
        this.kafkaProducer = kafkaProducer;
    }

    @Override
    public void transfer(Long transferId, Long profileId, Long debitAccountId,
                         Long creditAccountId, String currency, BigDecimal amount) {
        // Проверить, есть ли transferId в таблице Transfer
        // Если нет, то создать новую запись, записать данные в очередь  и вернуть Dto со статусом IN_PROGRESS
        // Если есть, то вернуть Dto с данными из БД
    }

    @Override
    public TransferResponseDto transfer(TransferRequestDto transferRequestDto) throws JsonProcessingException {
        // Проверить, есть ли transferId в таблице Transfer
        // Если нет, то создать новую запись, записать данные в очередь  и вернуть Dto со статусом IN_PROGRESS
        // Если есть, то вернуть Dto с данными из БД
        Transfer transfer;
        if (transferRequestDto.getTransferId() != null) {
            Optional<Transfer> transferOptional = transferRepository.findById(transferRequestDto.getTransferId());
            // todo обработать ошибку несуществ transferId
            transfer = transferOptional.orElse(null);
        } else {
            transfer = transferRepository.save(Transfer.builder()
                            .status("IN_PROGRESS")
                            .processedAt(LocalDateTime.now())
                            .build());
            kafkaProducer.sendMessage(transfer.getId(), transferRequestDto);
        }
        return null;
//        return TransferResponseDto.builder()
//                .id(transfer.getId())
//                .debitAccount(transfer.getDebitAccount().getId())
//                .debitCurrency(transfer.getDebitAccount().getCurrency().toString())
//                .debitAmount(transfer.getDebitAmount())
//                .debitExchangeRate(transfer.getDebitExchangeRate())
//                .creditAccount(transfer.getCreditAccount().getId())
//                .creditCurrency(transfer.getCreditAccount().getCurrency().toString())
//                .creditAmount(transfer.getCreditAmount())
//                .creditExchangeRate(transfer.getCreditExchangeRate())
//                .currency(transfer.getCurrency().toString())
//                .amount(transfer.getAmount())
//                .status(transfer.getStatus())
//                .message(transfer.getMessage())
//                .build();
    }
}
