package com.example.wallet.service;

import com.example.wallet.dto.AccountRequestDto;
import com.example.wallet.dto.AccountResponseDto;
import com.example.wallet.mapper.AccountMapper;
import com.example.wallet.model.Account;
import com.example.wallet.repository.AccountRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper mapper;

    @Override
    public AccountResponseDto save(AccountRequestDto requestDto) {
        Account account = mapper.requestDtoToEntity(requestDto);
        accountRepository.save(account);
        return mapper.entityToResponseDto(account);
    }

    @Override
    public AccountResponseDto update(Long id, AccountRequestDto requestDto) {
        Account account = mapper.requestDtoToEntity(requestDto);
        account.setId(id);
        accountRepository.save(account);
        return mapper.entityToResponseDto(account);
    }

    @Override
    public AccountResponseDto findById(Long id) {
        return mapper.entityToResponseDto(
                accountRepository.findById(id).orElse(null));
    }

    @Override
    public List<AccountResponseDto> findAll() {
        return accountRepository.findAll()
                .stream()
                .map(mapper::entityToResponseDto)
                .toList();
    }

    @Override
    public void delete(Long id) {
        accountRepository.deleteById(id);
    }
}
