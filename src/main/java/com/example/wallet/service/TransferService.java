package com.example.wallet.service;

import com.example.wallet.dto.TransferRequestDto;
import com.example.wallet.dto.TransferResponseDto;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.math.BigDecimal;

public interface TransferService {
    void transfer(Long transferId, Long profileId, Long debitAccountId,
                  Long creditAccountId, String currency, BigDecimal amount);

    TransferResponseDto transfer(TransferRequestDto transferRequestDto) throws JsonProcessingException;
}
