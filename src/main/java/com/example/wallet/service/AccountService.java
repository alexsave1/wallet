package com.example.wallet.service;

import com.example.wallet.dto.AccountRequestDto;
import com.example.wallet.dto.AccountResponseDto;

import java.util.List;

public interface AccountService {
    AccountResponseDto save(AccountRequestDto requestDto);

    AccountResponseDto update(Long id, AccountRequestDto requestDto);

    AccountResponseDto findById(Long id);

    List<AccountResponseDto> findAll();

    void delete(Long id);
}
