package com.example.wallet.service;

import com.example.wallet.dto.CurrencyRequestDto;
import com.example.wallet.dto.CurrencyResponseDto;
import com.example.wallet.mapper.CurrencyMapper;
import com.example.wallet.model.Currency;
import com.example.wallet.repository.AccountRepository;
import com.example.wallet.repository.CurrencyRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;
    private final AccountRepository accountRepository;
    private final CurrencyMapper mapper;

    @Override
    public CurrencyResponseDto save(CurrencyRequestDto requestDto) {
        Currency currency = mapper.requestDtoToEntity(requestDto);

        currencyRepository.save(currency);
        return mapper.entityToResponseDto(currency);
    }

    @Override
    public CurrencyResponseDto update(String id, CurrencyRequestDto requestDto) {
        Currency currency = mapper.requestDtoToEntity(requestDto);
        currency.setId(id);
        currencyRepository.save(currency);
        return mapper.entityToResponseDto(currency);
    }

    @Override
    public CurrencyResponseDto findById(String id) {
        return mapper.entityToResponseDto(
                currencyRepository.findById(id).orElse(null));
    }

    @Override
    public List<CurrencyResponseDto> findAll() {
        return currencyRepository.findAll()
                .stream()
                .map(mapper::entityToResponseDto)
                .toList();
    }

    @Override
    public void delete(String id) {
        accountRepository.deleteAccountsByCurrencyId(id);
        currencyRepository.deleteById(id);
    }
}
