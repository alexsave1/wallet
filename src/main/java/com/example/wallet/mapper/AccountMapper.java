package com.example.wallet.mapper;

import com.example.wallet.dto.AccountRequestDto;
import com.example.wallet.dto.AccountResponseDto;
import com.example.wallet.model.Account;

public interface AccountMapper {
    Account requestDtoToEntity(AccountRequestDto requestDto);

    AccountResponseDto entityToResponseDto(Account entity);
}
