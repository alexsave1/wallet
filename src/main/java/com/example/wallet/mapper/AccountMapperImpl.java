package com.example.wallet.mapper;

import com.example.wallet.dto.AccountRequestDto;
import com.example.wallet.dto.AccountResponseDto;
import com.example.wallet.model.Account;
import com.example.wallet.repository.CurrencyRepository;
import com.example.wallet.repository.ProfileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.processing.Generated;

@Generated(
        value = "org.mapstruct.ap.MappingProcessor",
        date = "2024-02-25T19:55:25+0300",
        comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.10 (Amazon.com Inc.)"
)
@Component
@RequiredArgsConstructor
public class AccountMapperImpl implements AccountMapper {

    private final CurrencyRepository currencyRepository;
    private final ProfileRepository profileRepository;

    @Override
    public Account requestDtoToEntity(AccountRequestDto requestDto) {
        if (requestDto == null) {
            return null;
        }

        Account.AccountBuilder account = Account.builder();

        account.balance(requestDto.getBalance());
        account.currency(currencyRepository.findById(requestDto.getCurrencyId()).orElse(null));
        account.profile(profileRepository.findById(requestDto.getProfileId()).orElse(null));

        return account.build();
    }

    @Override
    public AccountResponseDto entityToResponseDto(Account entity) {
        if (entity == null) {
            return null;
        }

        AccountResponseDto.AccountResponseDtoBuilder accountResponseDto = AccountResponseDto.builder();

        accountResponseDto.id(entity.getId());
        accountResponseDto.balance(entity.getBalance());
        accountResponseDto.currencyId(entity.getCurrency().getId());
        accountResponseDto.profileId(entity.getProfile().getId());
        accountResponseDto.isActive(entity.getIsActive());

        return accountResponseDto.build();
    }
}

