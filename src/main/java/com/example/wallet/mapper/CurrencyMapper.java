package com.example.wallet.mapper;

import com.example.wallet.dto.CurrencyRequestDto;
import com.example.wallet.dto.CurrencyResponseDto;
import com.example.wallet.model.Currency;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CurrencyMapper {
    Currency requestDtoToEntity(CurrencyRequestDto requestDto);

    CurrencyResponseDto entityToResponseDto(Currency entity);
}
