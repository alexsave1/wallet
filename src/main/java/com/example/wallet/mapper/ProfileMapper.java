package com.example.wallet.mapper;

import com.example.wallet.dto.ProfileRequestDto;
import com.example.wallet.dto.ProfileResponseDto;
import com.example.wallet.model.Profile;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProfileMapper {
    Profile requestDtoToEntity(ProfileRequestDto requestDto);

    ProfileResponseDto entityToResponseDto(Profile entity);
}
