package com.example.wallet.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {
    @KafkaListener(topics = "inward-topic", groupId = "wallet")
    public void listen(ConsumerRecord<Long, String> record) {
//        Long key = record.key();
//        String message = record.value();

//        System.out.println("Received message with key: " + key + ", value: " + message);
        System.out.println("test");
    }
}
