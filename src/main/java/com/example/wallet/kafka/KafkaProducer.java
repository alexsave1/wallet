package com.example.wallet.kafka;
import com.example.wallet.dto.TransferRequestDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class KafkaProducer {

    private final String TOPIC_NAME = "inward-topic";
    private final KafkaTemplate<Long, String> kafkaTemplate;

    public KafkaProducer(KafkaTemplate<Long, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendMessage(Long key, TransferRequestDto transferRequestDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(transferRequestDto);
        ProducerRecord<Long, String> record = new ProducerRecord<>(TOPIC_NAME, key, jsonString);
        kafkaTemplate.send(record);
    }

    public String sendMessage(String message) {
////        String key = UUID.randomUUID().toString();
//        String key = "some_key";
////        String message = "Hello Kafka!";
//        ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME, key, message);
//
////        kafkaTemplate.send(TOPIC_NAME, message);
//        kafkaTemplate.send(record);
//        System.out.println("Message sent to Kafka: " + message);
//        return key;
        return null;
    }
}