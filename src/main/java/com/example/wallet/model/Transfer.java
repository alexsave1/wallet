package com.example.wallet.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transfer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "debit_account_id")
    private Account debitAccount;
    @ManyToOne
    @JoinColumn(name = "debit_currency_id")
    private Currency debitCurrency;
    private BigDecimal debitAmount;
    private BigDecimal debitExchangeRate;

    @ManyToOne
    @JoinColumn(name = "credit_account_id")
    private Account creditAccount;
    @ManyToOne
    @JoinColumn(name = "credit_currency_id")
    private Currency creditCurrency;
    private BigDecimal creditAmount;
    private BigDecimal creditExchangeRate;

    @ManyToOne
    @JoinColumn(name = "currency_id")
    private Currency currency;
    private BigDecimal amount;
    private BigDecimal exchangeRate;
    private String status;
    private LocalDateTime processedAt;
    private String message;
}
